#ifndef SUBGENRE_H
#define SUBGENRE_H

// Librairies
#include <string>

/**
 *\file Subgenre.h
 *\brief Represent a Subgenre
 *\version 0.0.2
 *\author LECOMTE Victor
 */
class Subgenre
{
private:
    // Attributes
    unsigned int id; ///< Polyphony identifier
    short int sub_type; ///< Polyphonu sub_type as string

public:
    // Default constructors
    /**
     * \brief Default constructor
     */
    Subgenre();
    // Constructor with parameters
    /**
     * \brief Constructor with parameters
     * \param id : Gives the id of the subgenre entered by the user
     * \param sub_type : Gives the sub_type of the subgenre entered by the user
     */
    Subgenre(unsigned int id, short int sub_type);

    // Destructive
    /**
     * \brief Destructor
     */
    ~Subgenre();

    // Accessors (getters)
    /**
     * \brief Allows the recovery of the id
     */
    unsigned int GetId();
    /**
     * \brief Allows the recovery of the sub_type
     */
    short int getSub_Type();

    //  Mutators (setters)
    /**
     * \brief Allows the modification of the sub_type
     */
    void setSubtype(short int);
};
#endif //SUBGENRE_H
