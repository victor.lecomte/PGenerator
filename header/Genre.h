#ifndef GENRE_H
#define GENRE_H

// Librairies
#include <string>

/**
 * \file Genre.h
 * \brief Represent a Genre
 * \version 0.0.2
 * \author LECOMTE Victor && DAVID Corentin
 */
class Genre
{
private:
    // Attributes
    unsigned int id; ///< Format identifier
    std::string type; ///< Format type as string

public:
    // Default constructor
    /**
     * \brief Default constructor
     */
    Genre();

    // Constructor with parameters
    /**
     * \brief Constructor with parameter
     * \param id : Gives the id of the genre entered by the user
     * \param type : Gives the type of the genre entered by the user
     */
    Genre(unsigned int id, std::string type);

    // Destructive
    /**
     * \brief Destructor
     */
    ~Genre();

    // Accessors (getters)
    /**
     * \brief Allows the recovery of the id
     */
    unsigned int getId();
    /**
     * \brief Allow the recovery of the type
     */
    std::string getType();

    // Mutators (setters)
    /**
     * \brief Allows the modification of the type
     */
    void setType(std::string);
};
#endif // GENRE_H
