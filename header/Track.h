#ifndef TRACK_H
#define TRACK_H

// Librairies
#include <string>
#include <vector>
#include "Album.h"
#include "Artist.h"
#include "Format.h"
#include "Genre.h"
#include "Polyphony.h"
#include "Subgenre.h"

/**
 * \file Track.h
 * \brief Represent a Track
 * \version 0.0.2
 * \author LECOMTE Victor
 */
class Track
{
private:
    // Attributes
    unsigned int id; ///< Track identifier
    unsigned int duration; ///< Track duration as unsigned int
    std::string name; ///< Track nae as string
    std::string path; ///< Track path as string
    std::vector<Album> theAlbum;  ///< Track theAlbum as list of Album
    std::vector<Artist> theArtist; ///< Tack the Artist as list of Artist
    Album anAlbum; ///< Track anAbum as Abum
    Artist anArtist; ///< Track anArtist as Artist
    Format aFormat; ///< Track aFormat as Format
    Genre aGenre; ///< Track aGenre as Genre
    Subgenre aSubgenre; ///< Track aSubgenre as Subgenre
    Polyphony aPolyphony; ///< Track aPolyphony as Polyphony

public:
    //Default constructor
    /**
     * \brief Default constructor
     */
    Track();
    // Constructors with parameters
    /**
     * \brief Constructor with parameters with list of Album and list of Artist
     * \param id : Gives the id of the track entered by the user
     * \param duration : Gives the duration of the track entered by the user
     * \param name : Gives the name of the track entered by the user
     * \param path : Gives the id of the track
     * \param theAlbum : Gives a vector of album enterd by the user
     * \param theArtist : Gives a vector of artist entered by the user
     * \param aFormat : Instance of class Format
     * \param aGenre : Instance of class Genre
     * \param aSubgenre : Instance of class Subgenre
     * \param aPolyphony : Instance of class Polyphony
     */
    Track(unsigned int id, unsigned int duration, std::string name, std::string path, std::vector<Album> theAlbum, std::vector<Artist> theArtist, Format aFormat, Genre aGenre, Subgenre aSubgenre, Polyphony aPolyphony);
    /**
     * \brief Constructor with parameters
     * \param id : Gives the id of the track entered by the user
     * \param duration : Gives the duration of the track entered by the user
     * \param name : Gives the name of the track entered by the user
     * \param path : Gives the id of the track
     * \param anAlbum : Instance of class Album
     * \param anArtist : Instance of class Artist
     * \param aFormat : Instance of class Format
     * \param aGenre : Instance of class Genre
     * \param aSubgenre : Instance of class Subgenre
     * \param aPolyphony : Instance of class Polyphony
     */
    Track(unsigned int id, unsigned int duration, std::string name, std::string path, Album anAlbum, Artist anArtist, Format aFormat, Genre aGenre, Subgenre aSubgenre, Polyphony aPolyphony);

    // Destructive
    /**
     * \brief Destructor
     */
    ~Track();

    // Accessors (getters)
    /**
     * \brief Allows the recovery of the id
     */
    unsigned int getId();
    /**
     * \brief Allows the recovery of the duraation
     */
    unsigned int getDuration();
    /**
     * \brief Allows the recovery of the name
     */
    std::string getName();
    /**
     * \brief Allows the recovery of the path
     */
    std::string getPath();
    /**
     * \brief Allows the recovery of list of Album
     */
    std::vector<Album> getTheAlbum();
    /**
     * \brief Allows the recovery og list of Artist
     */
    std::vector<Artist> getTheArtist();
    /**
     * \brief Allows the recovery of Album class
     */
    Album getAlbum();
    /**
     * \brief Allows the recovery of Artist class
     */
    Artist getArtist();
    /**
     * \brief Allows the recovery of Format class
     */
    Format getFormat();
    /**
     * \brief Allows the recovery of Genre class
     */
    Genre getGenre();
    /**
     * \brief Allows the recovery of Subgenre class
     */
    Subgenre getSubgenre();
    /**
     * \brief Allows the recovery of Polyphony class
     */
    Polyphony getPolyphony();

    //  Mutators (setters)
    /**
     * \brief Allows the modification of the duration
     */
    void setDuration(unsigned int);
    /**
     * \brief Allows the modification of the name
     */
    void setName(std::string);
    /**
     * \brief Allows the modification of the path
     */
    void setPath(std::string);
    /**
     * \brief Allows the modification of Album class
     */
    void setTheAlbum(std::vector<Album>);
    /**
     * \brief Allows the modifiation of Artist class
     */
    void setTheArtist(std::vector<Artist>);
    /**
     * \brief Allows the modification of Album class
     */
    void setAlbum(Album);
    /**
     * \brief Allows the modification of Artist class
     */
    void setArtist(Artist);
    /**
     * \brief Allows the modification of Format class
     */
    void setFormat(Format);
    /**
     * \brief Allows the modification of Genre class
     */
    void setGenre(Genre);
    /**
     * \brief Allows the modification of Subgenre class
     */
    void setSubgenre(Subgenre);
    /**
     * \brief Allows the modification of Polyphony class
     */
    void setPolyphony(Polyphony);
};

#endif //TRACK_H
