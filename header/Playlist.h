#ifndef PLAYLIST_H
#define PLAYLIST_H

// Librairiess
#include <vector>
#include "Track.h"

/**
 * \file Playlist.h
 * \brief Represent a Playlist
 * \version 0.0.2
 * \author LECOMTE Victor
 */
class Playlist
{

private :
    //Attibutes
    unsigned int id; ///< Playlist identifier
    unsigned int duration; ///< Playlist duration
    std::vector<Track> tracks; ///< List of Track in a Playlist

public :
    // Default constructor
    /**
     * \brief Default constructor
     */
    Playlist();
    // Constructor with parameters
    /**
     * \brief Constructor with parameters
     * \param id : Gives the id of the playlist entered by the user
     * \param duration : Gives the duration of the playlist entered by the user
     * \param tracks : Gives a vector of tracks entered by the user
     */
    Playlist(unsigned int id, unsigned int duration, std::vector<Track> tracks);

    // Destructive
    /**
     * \brief Destructor
     */
    ~Playlist();

    // Accessors (getters)
    /**
    * \brief Allows the recovery of the id
    */
    unsigned int getId();
    /**
     * \brief Allows the recovery of the duration
     */
    unsigned int getDuration();
    /**
    * \brief Allows the recovery of a Track
    */
    std::vector<Track> getTrack();

    // Mutators (setters)
    /**
     * \brief Allows the modification of the duration
     */
    void setDuration(unsigned int);
    /**
     * \brief Allows the modification of a Track
     */
    void setTrack(std::vector<Track>);
};

#endif // PLAYLIST_H


