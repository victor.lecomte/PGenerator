// Librairies
#include <gflags/gflags.h>
#include <gflags/gflags_declare.h>

/**
 * Duration argument
 */
DECLARE_uint64(duration);
/**
 * Title argument
 */
DECLARE_string(title);
/**
 * Artist argument
 */
DECLARE_string(artist);
/**
 * Album argument
 */
DECLARE_string(album);
/**
 * Format argument
 */
DECLARE_bool(format);
/**
 * Genre argument
 */
DECLARE_string(genre);
/**
 * Polyphony argument
 */
DECLARE_uint64(polyphony);
/**
 * Subgenre argument
 */
DECLARE_string(subgenre);
