#ifndef ALBUM_H
#define ALBUM_H

// Librairies
#include <iostream>
#include <chrono>

/**
 * \file Album.h
 * \brief Represent an Album
 * \version 0.0.2
 * \author LECOMTE Victor
 */
class Album
{

private :
    unsigned int id; ///< Album identifier
    unsigned int date; ///< Album date as date
    std::string name; ///< Album name as string

public :
    // Default constructor
    /**
     * \brief Default constructor
     */
    Album();

    // Constructor with parameters
    /**
     * \brief Constructors with differents parameters
     * \param id : Gives the id of the album entered by the user
     * \param date : Gives the date of the album entered by the user
     * \param name : Gives the name  of the album entered by the user
     */
    Album(unsigned int id,  unsigned int date, std::string name);
    // Destructive
    /**
     * \brief Destuctor
     */
    ~Album();

    // Accessors (getters)
    /**
     * \brief Allows the recovery of the identifier
     */
    unsigned int getId();

    /**
     * \brief Allows the recovery of thre date
     */
    unsigned int getDate();
    /**
     * \brief Allows the recovery of the name
     */
    std::string getName();

    // Mutators (setters)
    /**
     * \brief Allows the modification of the date
     */
    void setDate(unsigned int);
    /**
     * \brief Allows the modification of the name
     */
    void setName(std::string);
};
#endif //ALBUM_H  
