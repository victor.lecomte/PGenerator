#include <iostream>
#include <cstring>
#include <string>
#include <libpq-fe.h>
#include <syslog.h>
#include "../header/PGenerator.h"


int connexion()
{
    openlog(NULL, LOG_CONS | LOG_PID, LOG_USER);
    const char *connexion_infos;
    PGconn *connexion;
    connexion_infos = "host='postgresql.bts-malraux72.net' port=5432 user='v.lecomte' dbname='Cours' connect_timeout=5";

    if(PQping(connexion_infos) == PQPING_OK)
    {
        connexion = PQconnectdb(connexion_infos);

        if(PQstatus(connexion) == CONNECTION_OK)
        {
            std::cout << "La connexion au serveur de base de données a été établie avec succès" << std::endl;
            syslog(LOG_INFO, "Success connection to postgreSQL");
            // TODO: #18
            PGresult *resultat = PQexec(connexion, "SELECT chemin FROM \"radio_libre\".\"morceau\";");
            ExecStatusType code_retour_execution = PQresultStatus(resultat);

            if(code_retour_execution == PGRES_TUPLES_OK)
            {
                char *valeur_champ;
                std::cout << "Voici la liste des chemins de tous les morceaux disponibles dans la base de données" << std::endl;

                for(unsigned int compteur_tuple = 0; (int)compteur_tuple < PQntuples(resultat); ++compteur_tuple)
                {
                    for(unsigned int compteur_champ = 0; (int)compteur_champ < PQnfields(resultat); ++compteur_champ)
                    {
                        valeur_champ = PQgetvalue(resultat, compteur_tuple, compteur_champ);
                        std::cout << valeur_champ << std::endl;
                    }
                }
            }
        }
        else
        {
            std::cout << "Malheuresement le serveur n'est pas joignable. Vérifié la connectivité" << std::endl;
            syslog(LOG_CRIT, "The server is not reachable. Check connectivity");
        }
    }

    closelog();
    return 0;
}
