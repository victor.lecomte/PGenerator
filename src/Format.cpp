//Librairies
#include "../header/Format.h"
#include <string>
#include <iostream>

//Default constructor
Format::Format() :
    id(),
    titule("M3U")

{
    std::cout << "Le constructeur par défault se compse de la façon suivant" << std::endl;
    std::cout << "L'id du format est : " << id << std::endl;
    std::cout << "Le titule du format est : " << titule << std::endl;
}

//Constructor with parameters
Format::Format(unsigned int  _id,
               std::string _titule
              ) :
    id(_id),
    titule(_titule)

{
    std::cout << "Constructor with parameters" << std::endl;
    std::cout << "L'id du format est : " << id << std::endl;
    std::cout << "Le titule du format est : " << titule << std::endl;
}

//Destructive
Format::~Format() {}

// Accessors (getters)
unsigned int Format::getId()
{
    return id;
}

std::string Format::getTitule()
{
    return titule;
}

// Mutators (setters)
void Format::setTitule(std::string _titule)
{
    titule = _titule;
}
