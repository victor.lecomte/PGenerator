#include <iostream>
#include <string>
#include <fstream>
#include <list>
#include "../header/XSPF.h"

using namespace std;

int xspf()
{
    ofstream fichier("playlist.xspf", ios::out | ios::trunc);
    string versionXSPF = "http://xspf.org/ns/0/";

    if(fichier) // if the opning is succeful
    {
        list<string> laListeMusique;
        laListeMusique.push_back("Sopra");
        laListeMusique.push_back("Eminem");
        laListeMusique.push_back("J Balvin");
        laListeMusique.push_back("Daddy Yankee");
        laListeMusique.push_back("Armin van Buuren");
        laListeMusique.push_back("DJ Assad");
        laListeMusique.push_back("Trust");
        laListeMusique.push_back("Louise Attaque");
        laListeMusique.push_back("Matmatah");
        laListeMusique.push_back("Mozart");

        // Browse the list
        for(string playlist : laListeMusique)
        {
            fichier << playlist << std::endl;
        }

        // We close the file
        fichier.close();
    }
    else  // otherwise
        cerr << "Erreur à l'ouverture !" << endl;

    return 0;

}
