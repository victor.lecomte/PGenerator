#include <iostream>
#include <string>
#include <fstream>
#include <list>
#include "../header/M3U.h"

using namespace std;

int m3u()
{
    ofstream fichier("playlist.m3u", ios::out | ios::trunc);// Declaration of the flow and opening of the file

    if(fichier)  // If the opening is succesfu
    {

        std::cout << "Here is the playlist that has just been generated after running the program" << std::endl; //
        list<string> laListe;
        laListe.push_back("Victor");
        laListe.push_back("Michou");
        laListe.push_back("Enzo");
        laListe.push_back("Alex");
        laListe.push_back("Florent");
        laListe.push_back("Momo");
        laListe.push_back("Dylan");
        laListe.push_back("LBJ");
        laListe.push_back("Rus Westbrook");
        laListe.push_back("ib BTS");
        // Browse the list
        for(string playlist : laListe)
        {
            fichier << playlist << std::endl;
        }

        // We close the file
        fichier.close();
    }
    else  // otherwise
        cerr << "Erreur à l'ouverture !" << endl;

    return 0;
}
