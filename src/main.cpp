#include <iostream>
#include <gflags/gflags.h>
#include "../header/connexion.h"
#include "../header/PGenerator.h"
#include "../header/M3U.h"
#include "../header/XSPF.h"
#include "testM3U.cpp"
#include "testXSPF.cpp"

int main(int argc, char *argv[]) {
    gflags::ParseCommandLineFlags(&argc,&argv,true);
    std::cout << "The duration of the request is : " << FLAGS_duration << std::endl;
    std::cout << "The title request is : " << FLAGS_title    << std::endl;
    std::cout << "The artist request is : " << FLAGS_artist    << std::endl;
    std::cout << "The album request is :  " << FLAGS_album << std::endl;
    std::cout << "The format request is :  "<< FLAGS_format << std::endl;
    std::cout << "The genre request is : " << FLAGS_genre << std::endl;
    std::cout << "The polyphony request is : " << FLAGS_polyphony << std::endl;
    std::cout << "The subgenre request is : "<< FLAGS_subgenre << std::endl;

    connexion();
    m3u();
    xspf();
    return 0;
}
