#include "../header/PGenerator.h"

DEFINE_uint64(duration, 60,  "The duration of the playlist");
DEFINE_string(title, "Great plalist", "Title of the playlist");
DEFINE_string(artist, "Eminem", "Artist of the playlist");
DEFINE_string(album, "album", "Album of the playlist");
DEFINE_bool(format, true, "Format M3U or XSPF");
DEFINE_string(genre, "rock", "Genre of the playlist");
DEFINE_uint64(polyphony, 5, "Polyphony of the playlist");
DEFINE_string(subgenre, " ", "Subgenre of the playlist");

//g++ -o test Test.cpp -I /usr/include/gflags -L /usr/lib/x86_64-linux-gnu -lgflags
